import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './pages/privatePages/dashboard/dashboard.component';
import { LoginComponent } from './pages/publicPages/login/login.component';
import { ProgressComponent } from './pages/privatePages/progress/progress.component';
import { GraphicComponent } from './pages/privatePages/graphic/graphic.component';
import { PrivatePagesComponent } from './pages/privatePages/private-pages.component';
import { AboutComponent } from './pages/publicPages/about/about.component';
import { HomeComponent } from './pages/publicPages/home/home.component';
import { PageNotFoundComponent } from './components/publicComponent/page-not-found/page-not-found.component';


const appRoutes: Routes = [
  {
    path: 'admin', component: PrivatePagesComponent,
    children: [
      { path: 'dashboard', component: DashboardComponent},
      { path: 'progress', component: ProgressComponent},
      { path: 'graphics', component: GraphicComponent},
      { path: '', redirectTo: '/admin/dashboard', pathMatch: 'full'},
    ]
  },
  { path: 'home', component: HomeComponent},
  { path: 'login', component: LoginComponent},
  { path: 'about', component: AboutComponent},
  { path: '', redirectTo: '/home', pathMatch: 'full'},
  { path: '**', component: PageNotFoundComponent}
];

export const APP_ROUTES = RouterModule.forRoot(appRoutes, {useHash: true, scrollPositionRestoration: 'enabled' });
