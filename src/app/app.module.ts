import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { APP_ROUTES } from './app.routes';

import { DashboardComponent } from './pages/privatePages/dashboard/dashboard.component';
import { ProgressComponent } from './pages/privatePages/progress/progress.component';
import { GraphicComponent } from './pages/privatePages/graphic/graphic.component';
import { HomeComponent } from './pages/publicPages/home/home.component';
import { AboutComponent } from './pages/publicPages/about/about.component';
import { ContactComponent } from './pages/publicPages/contact/contact.component';
import { EducativeModelComponent } from './pages/publicPages/educative-model/educative-model.component';
import { EducativeOfferComponent } from './pages/publicPages/educative-offer/educative-offer.component';
import { GalleryComponent } from './pages/publicPages/gallery/gallery.component';
import { LoginComponent } from './pages/publicPages/login/login.component';
import { NavbarComponent } from './components/publicComponent/navbar/navbar.component';
import { FooterComponent } from './components/publicComponent/footer/footer.component';
import { BreadcrumbComponent } from './components/privateComponent/breadcrumb/breadcrumb.component';
import { HeaderComponent } from './components/privateComponent/header/header.component';
import { SidebarComponent } from './components/privateComponent/sidebar/sidebar.component';
import { PrivatePagesComponent } from './pages/privatePages/private-pages.component';
import { PageNotFoundComponent } from './components/publicComponent/page-not-found/page-not-found.component';


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    ProgressComponent,
    GraphicComponent,
    HomeComponent,
    AboutComponent,
    ContactComponent,
    EducativeModelComponent,
    EducativeOfferComponent,
    GalleryComponent,
    LoginComponent,
    NavbarComponent,
    FooterComponent,
    BreadcrumbComponent,
    HeaderComponent,
    SidebarComponent,
    PrivatePagesComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    APP_ROUTES
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
